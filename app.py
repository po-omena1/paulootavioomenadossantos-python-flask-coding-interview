from flask import Flask


def create_app(**config_overrides):
    app = Flask(__name__)

    # apply overrides
    app.config.update(config_overrides)

    app.config['SECRET_KEY'] = 'J4AsoT6MtqYlwJNwo057RA'

    # setup db
    app.config['SQLALCHEMY_DATABASE_URI'] = \
        f"postgresql://jtuskfzs:QYpiXQ37dh3EnhaAfZCcO13xdwVytDbX@isilo.db.elephantsql.com/jtuskfzs"

    from apps.courses.routes import courses_app
    from apps.main.routes import main_app
    from apps.users.routes import user_app

    # register blueprints
    app.register_blueprint(main_app)
    app.register_blueprint(user_app)
    app.register_blueprint(courses_app)
    return app
