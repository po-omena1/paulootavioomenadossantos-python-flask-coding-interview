from db import db
from apps.courses.models import Courses


class Users(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String(100), nullable=False)
    last_name = db.Column(db.String(100), nullable=False)
    birth_date = db.Column(db.DateTime, nullable=False)
    email = db.Column(db.String(80), nullable=False)
    password = db.Column(db.String(256), nullable=False)
    is_active = db.Column(db.Boolean, default=True)
    enrollment_complete = db.Column(db.Boolean, default=False)
    courses = db.relationship('Course', secondary=Courses, lazy='subquery',
                              backref=db.backref('users', lazy=True))

    def __repr__(self):
        return '<Student {}  {}>'.format(self.first_name, self.last_name)
