from flask import Blueprint
from apps.main.utils import hc

main_app = Blueprint("main_app", __name__)


@main_app.route("/health-check", methods=("GET",))
def health_check():
    try:
        hc()
        return "OK"
    except Exception as e:
        return f"ERROR on database connection: {e}"


