from flask import Blueprint
from apps.courses.models import Courses
from db import db

courses_app = Blueprint("courses_app", __name__)


@courses_app.route("/api/v1/courses", methods=("GET",))
def courses():
    return [course.serialize() for course in Courses.query.all()]


@courses_app.route("/api/v1/courses/add", methods=("POST",))
def add_course():
    course = Courses()
    course.name = 'Test'
    course.max_students = 40
    course.professor_name = 'John Doe'

    db.session.add(course)
    db.session.commit()

    return 'Course added!'
