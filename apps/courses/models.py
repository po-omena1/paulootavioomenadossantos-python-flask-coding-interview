from db import db


class Courses(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), nullable=False)
    max_students = db.Column(db.Integer, nullable=False)
    professor_name = db.Column(db.String(100))

    def __repr__(self):
        return '<Course {}>'.format(self.name)

    def serialize(self):
        return {'id': self.id, 'name': self.name,
                'max_students': self.max_students,
                'professor_name': self.professor_name}

