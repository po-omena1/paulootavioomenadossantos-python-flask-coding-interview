from flask_migrate import Migrate

from app import create_app
from db import db
from apps.users.models import Users
from apps.courses.models import Courses

app = create_app()

db.init_app(app)
migrate = Migrate(app, db)

enrollment = db.Table('enrollment',
                      db.Column('course_id', db.Integer, db.ForeignKey('courses.id'), primary_key=True),
                      db.Column('user_id', db.Integer, db.ForeignKey('users.id'), primary_key=True)
                      )

with app.app_context():
    db.create_all()

# after creation
if __name__ == '__main__':
    app.run()

